import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomerTests {

    @Test
    public void MustAllowPersonalLoan() {
        System.out.println("Running: MustAllowPersonalLoan");

        var customer = new Customer("Wesley", "123.123.123-12", 27, "SP", 3000 );
        var loan = new Loan(customer);

        assertEquals(true, loan.personalLoanIsEnabled());
    }

    @Test
    public void MustAllowSecuredLoan() {
        System.out.println("Running: MustAllowSecuredLoan");

        var customer = new Customer("Wesley", "123.123.123-12", 27, "SP", 3000 );
        var loan = new Loan(customer);

        assertEquals(true, loan.securedLoanIsEnabled());
    }

    @Test
    public void MustNotAllowSecuredLoan() {
        System.out.println("Running: MustNotAllowSecuredLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "BH", 3000 );
        var loan = new Loan(customer);

        assertEquals(false, loan.securedLoanIsEnabled());
    }

    @Test
    public void MustAllowConsignedLoan() {
        System.out.println("Running: MustAllowConsignedLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "SP", 5000 );
        var loan = new Loan(customer);

        assertEquals(true, loan.consignedLoanIsEnabled());
    }

    @Test
    public void ShouldReturn240inInstallmentValue() {
        System.out.println("Running: MustAllowConsignedLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "SP", 5000 );
        var loan = new Loan(customer);
        var proposal = loan.generatePersonalLoanProposal(1000, 5);

        double installmentExpected = 240;

        assertEquals(true, installmentExpected == proposal.getInstallmentsValue());
    }

    @Test
    public void ShouldReturnASecuredLoan() {
        System.out.println("Running: MustAllowConsignedLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "SP", 3001 );
        var loan = new Loan(customer);
        var proposal = loan.generateSecuredLoanProposal(1000, 5);

        double installmentExpected = 230;

        assertEquals(true, installmentExpected == proposal.getInstallmentsValue());
    }

    @Test
    public void ShouldReturnAConsignedLoan() {
        System.out.println("Running: MustAllowConsignedLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "SP", 5000);
        var loan = new Loan(customer);
        var proposal = loan.generateConsignedLoanProposal(1000, 5);

        double installmentExpected = 220;

        assertEquals(true, installmentExpected == proposal.getInstallmentsValue());
    }

    @Test
    public void ShouldReturnAllValuesOfLoan() {
        System.out.println("Running: MustAllowConsignedLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "SP", 5000);
        var loan = new Loan(customer);
        var proposal = loan.generateConsignedLoanProposal(1000, 5);

        assertEquals(true, 1000 == proposal.getLeanAmount());
        assertEquals(LoanType.CONSIGNED, proposal.getLoanType());
        assertEquals(customer, proposal.getCustomer());

        double installmentExpected = 220;

        assertEquals(true, installmentExpected == proposal.getInstallmentsValue());
    }

    @Test
    public void MustReturnApayrollLoan()
    {
        System.out.println("Running: MustAllowConsignedLoan");

        var loanType = LoanType.CONSIGNED;

        System.out.println(loanType);

        Assert.assertEquals(true, 0.02 == loanType.getPercentage());
    }

    @Test
    public void MustReturnACustomer()
    {
        var customer = new Customer("Wesley", "123.123.123-12", 27, "SP", 3000);
        Assert.assertEquals("Wesley", customer.getName());
        Assert.assertEquals("123.123.123-12", customer.getCpf());
        Assert.assertEquals("SP", customer.getLocation());
        Assert.assertEquals(3000, customer.getIncome());
    }

    @Test
    public void MustReturnProposalNull()
    {
        System.out.println("Running: MustAllowConsignedLoan");

        var customer = new Customer("Wesley Jesus", "123.123.123-12", 29, "SP", 3000);
        var loan = new Loan(customer);
        var proposal = loan.generateConsignedLoanProposal(1000, 5);

        assertEquals(null, proposal);
    }
}