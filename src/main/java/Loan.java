public class Loan {

    private final Customer customer;
    private final double leanAmount; // valor de emprestimo
    private final double installmentsValue; // valor das parcelas
    private final LoanType loanType; // tipo do emprestimo

    public Loan(Customer customer) {
        this.customer = customer;
        leanAmount = 0;
        installmentsValue = 0;
        loanType = LoanType.PERSONAL; // default
    }

    private Loan(Customer customer, double leanAmount, double installmentsValue, LoanType loanType)
    {
        this.customer = customer;
        this.leanAmount = leanAmount;
        this.installmentsValue = installmentsValue;
        this.loanType = loanType;
    }

    public double getInstallmentsValue() {
        return this.installmentsValue;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public double getLeanAmount() {
        return leanAmount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean personalLoanIsEnabled () {
        return minimumIncomePersonalLoanIsValid();
    }

    public boolean securedLoanIsEnabled() {
        if(minimumIncomeSecuredLoanIsValid()) return true;

        if(mediumIncomeSecuredLoanIsValid()) return true;

        if(highIncomeSecuredLoanIsValid()) return true;

        return false;
    }

    public boolean consignedLoanIsEnabled() {
        return minimumIncomeConsignedLoanIsValid();
    }

    private boolean minimumIncomePersonalLoanIsValid() {
        return customer.getIncome() > 0;
    }

    private boolean minimumIncomeSecuredLoanIsValid() {
        return customer.getIncome() > 0 && customer.getIncome() <= 3000 && customer.getAge() < 30 && customer.getLocation() == "SP";
    }

    private boolean mediumIncomeSecuredLoanIsValid() {
        return customer.getIncome() > 3000 && this.customer.getIncome() < 5000 && customer.getLocation() == "SP";
    }

    private boolean highIncomeSecuredLoanIsValid() {
        return customer.getIncome() >= 5000 && customer.getAge() < 30;
    }

    private boolean minimumIncomeConsignedLoanIsValid() {
        return customer.getIncome() >= 5000 && customer.getAge() < 30;
    }

    private double calculateTotalInterest(double leanAmount, double percentage) {
        var calculated =  leanAmount * percentage;
        return  calculated;
    }

    private double calculateValueOfInstallment(double leanAmout, int totalInstallment, LoanType loanType) {
        var installmentValue = leanAmout / totalInstallment;
        return installmentValue + calculateTotalInterest(leanAmout, loanType.getPercentage());
    }

    public Loan generatePersonalLoanProposal(double leanAmount, int totalInstallment)
    {
        if(personalLoanIsEnabled())
            return new Loan(
                this.customer,
                leanAmount,
                calculateValueOfInstallment(leanAmount, totalInstallment, LoanType.PERSONAL),
                LoanType.PERSONAL);

        return null;
    }

    public Loan generateSecuredLoanProposal(double leanAmount, int totalInstallment)
    {
        if(securedLoanIsEnabled())
            return new Loan(
                    this.customer,
                    leanAmount,
                    calculateValueOfInstallment(leanAmount, totalInstallment, LoanType.SECURED_LOAN),
                    LoanType.SECURED_LOAN);

        return null;
    }

    public Loan generateConsignedLoanProposal(double leanAmount, int totalInstallment)
    {
        if(consignedLoanIsEnabled())
            return new Loan(
                    this.customer,
                    leanAmount,
                    calculateValueOfInstallment(leanAmount, totalInstallment, LoanType.CONSIGNED),
                    LoanType.CONSIGNED);

        return null;
    }
}