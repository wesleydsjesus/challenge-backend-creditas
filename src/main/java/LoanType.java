public enum LoanType {
    PERSONAL (0.04),
    SECURED_LOAN(0.03),
    CONSIGNED(0.02);

    private double percentage;

    private LoanType(double percentage) {
        this.percentage = percentage;
    }

    public double getPercentage() {
        return this.percentage;
    }
}
