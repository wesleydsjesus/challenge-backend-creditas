public class Customer {

    // todo: renda

    private final String name;

    private final String cpf;

    private final int age;

    private final String location;

    private final int income;

    public Customer(String name, String cpf, int age, String location, int income) {
        this.name = name;
        this.cpf = cpf;
        this.age = age;
        this.location = location;
        this.income = income;
    }
    public String getName() {
        return this.name;
    }

    public String getCpf() {
       return this.cpf;
    }

    public int getAge() {
        return this.age;
    }

    public String getLocation() {
        return this.location;
    }

    public int getIncome() {
        return this.income;
    }

}
